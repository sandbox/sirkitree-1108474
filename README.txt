*NOTE: this is outdated. While it was a good starting point, we've moved on. Please see https://drupal.org/node/1018084 instead.

Details on how this repo is setup:
==================================
.git/
.gitmodules/
drupal/      <-- a Git submodule
libraries/   <-- contains various git submodules
modules/     <-- contains various git submodules
themes/      <-- contains various git submodules


Working with this repo:
=======================
$ git clone <url>
$ git submodule update --init

Adding contrib modules:
-----------------------
$ git submodule add <url>/$1.git modules/$1

ex: $ git submodule add http://git.drupal.org/project/features.git modules/features

Symbolic links:
---------------
You'll have to set these up yourself after cloning if on windows. If you're on
Linux or Mac, you can run the build.sh script provided, which creates these
for you.

sites/all/libraries@ -> ../../../libraries
sites/all/modules@ -> ../../../modules
sites/all/themes@ -> ../../../themes

Database dumps:
---------------
If you would like a current database dump from the scratch site to use on your
local, please get in touch with one of the following people:
 - sirkitree
 - coltrane
 - greggles
 - kenwoodworth
 - sreyen