#!/bin/sh

# Usage: $ ./build.sh

# Setup symlinks

## Installation Profile

## Modules
rm -rf drupal/sites/all/modules
ln -s ../../../modules drupal/sites/all/modules

## Libraries
ln -s ../../../libraries drupal/sites/all/libraries

## Themes
rm -rf drupal/sites/all/themes
ln -s ../../../themes drupal/sites/all/themes

# Create settings.php
cp drupal/sites/default/default.settings.php drupal/sites/default/settings.php