
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>DrupalCon Denver 2012</title>
  <link rel="stylesheet" type="text/css" href="/sites/all/themes/denver/fonts/steelfish.css" />
  <link rel="stylesheet" type="text/css" href="/sites/all/themes/denver/css/placeholder.css" />
  <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>

<body id="home">
  <section id="content">
    <section id="slide1" class="slide">
      <img class="drop" src="/sites/all/themes/denver/images/drop.png" alt="Drupalcon Denver logo">
      <h1>DrupalCon Denver</h1>
      <h2>March 20 - 22, 2012</h2>
    </section>
    <section id="drupalElevated">
      <section id="slide2" class="slide">
        <div class="content">
          <h2>Drupal Elevated</h2>
          <p>Over the past 10 years, Drupal has moved from a meager social networking platform built for fun by a Computer Science major in college, to a formidable platform powering massive websites for government, commerce, non-profit, entertainment, and virtually every other sector.</p>
        </div>
        <img id="linuxJournal" src="/sites/all/themes/denver/images/drupalElevated-linuxJournal.png" alt="Angela Byron on Linux Journal cover">
        <img id="dcfsPhoto" src="/sites/all/themes/denver/images/drupalElevated-dcsfPhoto.png" alt="Drupalcon San Fransisco group photo">
        <img id="launchParties" src="/sites/all/themes/denver/images/drupalElevated-launchParties.png" alt="Drupal 7 launch parties map">
        <img id="drupalWebsite" src="/sites/all/themes/denver/images/drupalElevated-drupalWebsite.png" alt="Drupal.org redesign">
        <div id="beyond">
          <img id="badge" src="/sites/all/themes/denver/images/drupalElevated-badge.png" alt="Celebrating 10 years of Drupal">
          <h3>Beyond 10 Years</h3>
          <p>Powered by an active, passionate community, Drupal will continue to solve important technology problems.</p>
        </div>
      </section>
    </section>
    <section id="denver">
      <section id="slide3" class="slide">
        <h2>Denver - The Mile High City</h2>
        <div class="content">
          <p>What better place to celebrate Drupal Elevated than the Mile High City of Denver, Colorado. From its championship-winning sports teams; to its incredible range of locally-brewed craft beers; to its vibrant downtown full of shops, hotels, galleries, restaurants, and the like; to the world-class skiing nearby; Denver has a lot to offer.</p>
        </div>
        <div class="gallery">
          <ul class="photos">
            <li class="current"><img src="/sites/all/themes/denver/images/denver-photo-01.jpg" alt="skiing and snowboarding"></li>
            <li><img src="/sites/all/themes/denver/images/denver-photo-02.jpg" alt="sports"></li>
            <li><img src="/sites/all/themes/denver/images/denver-photo-03.jpg" alt="music"></li>
            <li><img src="/sites/all/themes/denver/images/denver-photo-04.jpg" alt="beer"></li>
            <li><img src="/sites/all/themes/denver/images/denver-photo-05.jpg" alt="food"></li>
          </ul>
        </div>
      </section>
      <section id="slide4" class="slide">
        <div class="content">
          <h2>Flight Times <small>(in hours)</small></h2>
          <p>Denver is accessible.  It is home to the largest international airport in the United States with direct flights from most major cities, and is less than four hours by air from either coast.</p>
        </div>
        <img id="map" src="/sites/all/themes/denver/images/location-map.jpg" alt="flight times to Denver">
      </section>
      <section id="slide5" class="slide">
        <div class="content">
          <h2><strong>Denver</strong> is good for <strong>Business</strong></h2>
          <p>with major operations from some of the largest manufacturing, telecom, energy and defense companies in the US, a strong focus on technology, and a vibrant tech-startup culture.</p>
        </div>
        <img id="business01" src="/sites/all/themes/denver/images/business-photo-01.jpg" alt="Denver business">
        <img id="business02" src="/sites/all/themes/denver/images/business-photo-02.jpg" alt="Denver business">
      </section>
    </section>
    <section id="conventionCenter">
      <section id="slide6" class="slide">
        <img id="business03" src="/sites/all/themes/denver/images/business-photo-03.png" alt="Denver business">
        <h2>The Colorado Convention Center</h2>
        <div class="content">
          <p>DrupalCon 2012 will occupy the massive, well-appointed Colorado Convention Center, located in the heart of downtown Denver.  When not in sessions, enjoy Denver cuisine at one the many locally-owned restaurants on Larimer street, catch a show at the country's second largest performing arts center next door, or explore world-class galleries at the Denver Arts Museum, an architectural landmark just a few blocks away.  Galleries, shops, restaurants, live music, bars, and hotels are just a few of the attractions nearby.</p>
          <p>Everything you need is within walking distance.</p>
        </div>
        <div class="gallery">
          <ul class="photos">
            <li class="current"><img src="/sites/all/themes/denver/images/conventionCenter-photo-01.jpg" alt="Colorado Convention Center"></li>
            <li><img src="/sites/all/themes/denver/images/conventionCenter-photo-02.jpg" alt="Colorado Convention Center"></li>
            <li><img src="/sites/all/themes/denver/images/conventionCenter-photo-03.jpg" alt="Colorado Convention Center"></li>
            <li><img src="/sites/all/themes/denver/images/conventionCenter-photo-04.jpg" alt="Colorado Convention Center"></li>
          </ul>
        </div>
      </section>
    </section>
    <section id="community">
      <section id="slide7" class="slide">
        <div class="content">
          <h2>The Colorado Drupal Community</h2>
          <p>The Drupal Community in Colorado is strong.  Our unique, active community is thrilled to host the next North American DrupalCon, and welcomes the opportunity to connect so directly with the international Drupal community.</p>
        </div>
        <dl id="community-stats">
          <dt id="stat01-dt">400<span>+</span></dt>
          <dd id="stat01-dd">people at DrupalCamp Colorado 2011</dd>
          <dt id="stat02-dt">4</dt>
          <dd id="stat02-dd">monthly meetups</dd>
          <dt id="stat03-dt">35<span>+</span></dt>
          <dd id="stat03-dd">people in #drupal-colorado</dd>
          <dt id="stat04-dt">100%</dt>
          <dd id="stat04-dd">AWESOME</dd>
        </dl>
      </section>
      <section id="slide8" class="slide">
        <div class="content">
          <h2>Be a Part of <strong>DrupalCon 2012</strong></h2>
          <p>We want your help. To stay informed and help shape Drupal 2012, <a href="http://twitter.com/drupalcondenver">follow us on twitter</a> and visit this website frequently.</p>
        </div>
        <!-- Begin MailChimp Signup Form -->
        <script type="text/javascript">
        // delete this script tag and use a "div.mce_inline_error{ XXX !important}" selector
        // or fill this in and it will be inlined when errors are generated
        var mc_custom_error_style = '';
        </script>
        <div id="mc_embed_signup">
          <form action="http://drupal.us2.list-manage1.com/subscribe/post?u=618af7a460cf93cf211231139&amp;id=189afc7b7a" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">
            <fieldset>
              <legend><span>Join our mailing list</span></legend>
              <div class="indicate-required">* required</div>
              <div class="mc-field-group">
                <label for="mce-EMAIL">Email Address <strong class="note-required">*</strong></label>
                <input type="text" value="" name="EMAIL" class="required email form-text" id="mce-EMAIL">
              </div>
              <div class="mc-field-group">
                <label for="mce-NAME">Name </label>
                <input type="text" value="" name="NAME" class="form-text" id="mce-NAME">
              </div>
              <div class="mc-field-group">
                <label for="mce-COMPANY">Company </label>
                <input type="text" value="" name="COMPANY" class="form-text" id="mce-COMPANY">
              </div>
            <div class="mc-field-group">
              <label for="mce-COUNTRY">Location </label>
              <select name="COUNTRY" class="form-select" id="mce-COUNTRY">
                <option value=""></option>
                <option value="United States of America">United States of America</option>
                <option value="Aaland Islands">Aaland Islands</option>
                <option value="Afghanistan">Afghanistan</option>
                <option value="Albania">Albania</option>
                <option value="Algeria">Algeria</option>
                <option value="American Samoa">American Samoa</option>
                <option value="Andorra">Andorra</option>
                <option value="Angola">Angola</option>
                <option value="Anguilla">Anguilla</option>
                <option value="Anguilla">Anguilla</option>
                <option value="Antarctica">Antarctica</option>
                <option value="Antigua And Barbuda">Antigua And Barbuda</option>
                <option value="Argentina">Argentina</option>
                <option value="Armenia">Armenia</option>
                <option value="Aruba">Aruba</option>
                <option value="Australia">Australia</option>
                <option value="Austria">Austria</option>
                <option value="Azerbaijan">Azerbaijan</option>
                <option value="Bahamas">Bahamas</option>
                <option value="Bahrain">Bahrain</option>
                <option value="Bangladesh">Bangladesh</option>
                <option value="Barbados">Barbados</option>
                <option value="Belarus">Belarus</option>
                <option value="Belgium">Belgium</option>
                <option value="Belize">Belize</option>
                <option value="Benin">Benin</option>
                <option value="Bermuda">Bermuda</option>
                <option value="Bhutan">Bhutan</option>
                <option value="Bolivia">Bolivia</option>
                <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                <option value="Botswana">Botswana</option>
                <option value="Bouvet Island">Bouvet Island</option>
                <option value="Brazil">Brazil</option>
                <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                <option value="Brunei Darussalam">Brunei Darussalam</option>
                <option value="Bulgaria">Bulgaria</option>
                <option value="Burkina Faso">Burkina Faso</option>
                <option value="Burundi">Burundi</option>
                <option value="Cambodia">Cambodia</option>
                <option value="Cameroon">Cameroon</option>
                <option value="Canada">Canada</option>
                <option value="Cape Verde">Cape Verde</option>
                <option value="Cayman Islands">Cayman Islands</option>
                <option value="Central African Republic">Central African Republic</option>
                <option value="Chad">Chad</option>
                <option value="Chile">Chile</option>
                <option value="China">China</option>
                <option value="Christmas Island">Christmas Island</option>
                <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                <option value="Colombia">Colombia</option>
                <option value="Comoros">Comoros</option>
                <option value="Congo">Congo</option>
                <option value="Cook Islands">Cook Islands</option>
                <option value="Costa Rica">Costa Rica</option>
                <option value="Costa Rica">Costa Rica</option>
                <option value="Cote D'Ivoire">Cote D'Ivoire</option>
                <option value="Croatia">Croatia</option>
                <option value="Cuba">Cuba</option>
                <option value="Cyprus">Cyprus</option>
                <option value="Czech Republic">Czech Republic</option>
                <option value="Denmark">Denmark</option>
                <option value="Djibouti">Djibouti</option>
                <option value="Dominica">Dominica</option>
                <option value="Dominica">Dominica</option>
                <option value="Dominican Republic">Dominican Republic</option>
                <option value="East Timor">East Timor</option>
                <option value="Ecuador">Ecuador</option>
                <option value="Egypt">Egypt</option>
                <option value="El Salvador">El Salvador</option>
                <option value="Equatorial Guinea">Equatorial Guinea</option>
                <option value="Eritrea">Eritrea</option>
                <option value="Estonia">Estonia</option>
                <option value="Ethiopia">Ethiopia</option>
                <option value="Falkland Islands">Falkland Islands</option>
                <option value="Faroe Islands">Faroe Islands</option>
                <option value="Fiji">Fiji</option>
                <option value="Finland">Finland</option>
                <option value="France">France</option>
                <option value="French Guiana">French Guiana</option>
                <option value="French Polynesia">French Polynesia</option>
                <option value="French Southern Territories">French Southern Territories</option>
                <option value="Gabon">Gabon</option>
                <option value="Gambia">Gambia</option>
                <option value="Germany">Germany</option>
                <option value="Ghana">Ghana</option>
                <option value="Gibraltar">Gibraltar</option>
                <option value="Greece">Greece</option>
                <option value="Greenland">Greenland</option>
                <option value="Grenada">Grenada</option>
                <option value="Guadeloupe">Guadeloupe</option>
                <option value="Guam">Guam</option>
                <option value="Guatemala">Guatemala</option>
                <option value="Guernsey">Guernsey</option>
                <option value="Guinea">Guinea</option>
                <option value="Guinea-Bissau">Guinea-Bissau</option>
                <option value="Guyana">Guyana</option>
                <option value="Haiti">Haiti</option>
                <option value="Heard and Mc Donald Islands">Heard and Mc Donald Islands</option>
                <option value="Honduras">Honduras</option>
                <option value="Hong Kong">Hong Kong</option>
                <option value="Hungary">Hungary</option>
                <option value="Iceland">Iceland</option>
                <option value="India">India</option>
                <option value="Indonesia">Indonesia</option>
                <option value="Iran">Iran</option>
                <option value="Iraq">Iraq</option>
                <option value="Ireland">Ireland</option>
                <option value="Israel">Israel</option>
                <option value="Italy">Italy</option>
                <option value="Jamaica">Jamaica</option>
                <option value="Japan">Japan</option>
                <option value="Jersey  (Channel Islands)">Jersey  (Channel Islands)</option>
                <option value="Jordan">Jordan</option>
                <option value="Kazakhstan">Kazakhstan</option>
                <option value="Kenya">Kenya</option>
                <option value="Kiribati">Kiribati</option>
                <option value="Kuwait">Kuwait</option>
                <option value="Kuwait">Kuwait</option>
                <option value="Kyrgyzstan">Kyrgyzstan</option>
                <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                <option value="Latvia">Latvia</option>
                <option value="Lebanon">Lebanon</option>
                <option value="Lesotho">Lesotho</option>
                <option value="Liberia">Liberia</option>
                <option value="Libya">Libya</option>
                <option value="Liechtenstein">Liechtenstein</option>
                <option value="Lithuania">Lithuania</option>
                <option value="Luxembourg">Luxembourg</option>
                <option value="Macau">Macau</option>
                <option value="Macedonia">Macedonia</option>
                <option value="Madagascar">Madagascar</option>
                <option value="Malawi">Malawi</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Maldives">Maldives</option>
                <option value="Mali">Mali</option>
                <option value="Malta">Malta</option>
                <option value="Marshall Islands">Marshall Islands</option>
                <option value="Martinique">Martinique</option>
                <option value="Mauritania">Mauritania</option>
                <option value="Mauritius">Mauritius</option>
                <option value="Mayotte">Mayotte</option>
                <option value="Mexico">Mexico</option>
                <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                <option value="Moldova, Republic of">Moldova, Republic of</option>
                <option value="Monaco">Monaco</option>
                <option value="Mongolia">Mongolia</option>
                <option value="Montenegro">Montenegro</option>
                <option value="Montserrat">Montserrat</option>
                <option value="Morocco">Morocco</option>
                <option value="Mozambique">Mozambique</option>
                <option value="Myanmar">Myanmar</option>
                <option value="Namibia">Namibia</option>
                <option value="Nauru">Nauru</option>
                <option value="Nepal">Nepal</option>
                <option value="Netherlands">Netherlands</option>
                <option value="Netherlands Antilles">Netherlands Antilles</option>
                <option value="New Caledonia">New Caledonia</option>
                <option value="New Zealand">New Zealand</option>
                <option value="Nicaragua">Nicaragua</option>
                <option value="Niger">Niger</option>
                <option value="Nigeria">Nigeria</option>
                <option value="Niue">Niue</option>
                <option value="Norfolk Island">Norfolk Island</option>
                <option value="North Korea">North Korea</option>
                <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                <option value="Norway">Norway</option>
                <option value="Oman">Oman</option>
                <option value="Pakistan">Pakistan</option>
                <option value="Palau">Palau</option>
                <option value="Palestine">Palestine</option>
                <option value="Panama">Panama</option>
                <option value="Papua New Guinea">Papua New Guinea</option>
                <option value="Paraguay">Paraguay</option>
                <option value="Peru">Peru</option>
                <option value="Philippines">Philippines</option>
                <option value="Pitcairn">Pitcairn</option>
                <option value="Poland">Poland</option>
                <option value="Portugal">Portugal</option>
                <option value="Puerto Rico">Puerto Rico</option>
                <option value="Qatar">Qatar</option>
                <option value="Republic of Georgia">Republic of Georgia</option>
                <option value="Reunion">Reunion</option>
                <option value="Romania">Romania</option>
                <option value="Russia">Russia</option>
                <option value="Russia">Russia</option>
                <option value="Rwanda">Rwanda</option>
                <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                <option value="Saint Lucia">Saint Lucia</option>
                <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                <option value="Samoa (Independent)">Samoa (Independent)</option>
                <option value="San Marino">San Marino</option>
                <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                <option value="Saudi Arabia">Saudi Arabia</option>
                <option value="Senegal">Senegal</option>
                <option value="Serbia">Serbia</option>
                <option value="Serbia">Serbia</option>
                <option value="Seychelles">Seychelles</option>
                <option value="Sierra Leone">Sierra Leone</option>
                <option value="Singapore">Singapore</option>
                <option value="Slovakia">Slovakia</option>
                <option value="Slovenia">Slovenia</option>
                <option value="Solomon Islands">Solomon Islands</option>
                <option value="Somalia">Somalia</option>
                <option value="South Africa">South Africa</option>
                <option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
                <option value="South Korea">South Korea</option>
                <option value="South Korea">South Korea</option>
                <option value="Spain">Spain</option>
                <option value="Sri Lanka">Sri Lanka</option>
                <option value="St. Helena">St. Helena</option>
                <option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>
                <option value="Sudan">Sudan</option>
                <option value="Sudan">Sudan</option>
                <option value="Suriname">Suriname</option>
                <option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
                <option value="Swaziland">Swaziland</option>
                <option value="Sweden">Sweden</option>
                <option value="Switzerland">Switzerland</option>
                <option value="Syria">Syria</option>
                <option value="Taiwan">Taiwan</option>
                <option value="Tajikistan">Tajikistan</option>
                <option value="Tanzania">Tanzania</option>
                <option value="Thailand">Thailand</option>
                <option value="Togo">Togo</option>
                <option value="Tokelau">Tokelau</option>
                <option value="Tonga">Tonga</option>
                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                <option value="Tunisia">Tunisia</option>
                <option value="Turkey">Turkey</option>
                <option value="Turkmenistan">Turkmenistan</option>
                <option value="Turks &amp; Caicos Islands">Turks &amp; Caicos Islands</option>
                <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                <option value="Tuvalu">Tuvalu</option>
                <option value="Uganda">Uganda</option>
                <option value="Ukraine">Ukraine</option>
                <option value="United Arab Emirates">United Arab Emirates</option>
                <option value="United Kingdom">United Kingdom</option>
                <option value="United Kingdom">United Kingdom</option>
                <option value="Uruguay">Uruguay</option>
                <option value="USA Minor Outlying Islands">USA Minor Outlying Islands</option>
                <option value="Uzbekistan">Uzbekistan</option>
                <option value="Vanuatu">Vanuatu</option>
                <option value="Vatican City State (Holy See)">Vatican City State (Holy See)</option>
                <option value="Venezuela">Venezuela</option>
                <option value="Vietnam">Vietnam</option>
                <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                <option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
                <option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
                <option value="Western Sahara">Western Sahara</option>
                <option value="Yemen">Yemen</option>
                <option value="Zaire">Zaire</option>
                <option value="Zambia">Zambia</option>
                <option value="Zimbabwe">Zimbabwe</option>
              </select>
            </div>
            <div class="mc-field-group">
              <label class="input-group-label">I am... </label>
              <div class="input-group">
              <ul>
                <li><input type="checkbox" value="1" name="group[5][1]" id="mce-group[5]-5-0"><label for="mce-group[5]-5-0">a developer</label></li>
                <li><input type="checkbox" value="2" name="group[5][2]" id="mce-group[5]-5-1"><label for="mce-group[5]-5-1">a designer</label></li>
                <li><input type="checkbox" value="4" name="group[5][4]" id="mce-group[5]-5-2"><label for="mce-group[5]-5-2">a user</label></li>
                <li><input type="checkbox" value="8" name="group[5][8]" id="mce-group[5]-5-3"><label for="mce-group[5]-5-3">a sponsor</label></li>
                <li><input type="checkbox" value="16" name="group[5][16]" id="mce-group[5]-5-4"><label for="mce-group[5]-5-4">interested in volunteering</label></li>
              </ul>
            </div>
          </div>
          <div id="mce-responses">
            <div class="response" id="mce-error-response" style="display:none"></div>
            <div class="response" id="mce-success-response" style="display:none"></div>
          </div>
          <div><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn"></div>
        </fieldset>
      </form>
    </div>

        <!--End mc_embed_signup-->
      </section>
    </section>
    <section id="slide9" class="slide">
      <img class="drop" src="/sites/all/themes/denver/images/drop.png" alt="Drupalcon Denver logo">
      <h1>DrupalCon Denver</h1>
      <h2>March 20 - 22, 2012</h2>
      <a href="http://twitter.com/drupalcondenver" class="twitter">Follow <strong>@drupalcondenver</strong> on Twitter</a>
    </section>
    <nav id="nav">
      <div class="container">
        <ul>
          <li><a href="#drupalElevated">Drupal Elevated</a></li>
          <li><a href="#denver">Denver</a></li>
          <li><a href="#conventionCenter">The Convention Center</a></li>
          <li><a href="#community">The Community</a></li>
        </ul>
        <a href="http://twitter.com/drupalcondenver" class="twitter">Follow <strong>@drupalcondenver</strong> on Twitter</a>
      </div>
    </nav>
    <img id="bg-mountains" class="full-image" src="/sites/all/themes/denver/images/bg-mountains.jpg" alt="Colorado mountains">
  </section>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
  <script src="/sites/all/themes/denver/js/drupalcon.js"></script>
  <script  type="text/javascript">
  try {
      var jqueryLoaded=jQuery;
      jqueryLoaded=true;
  } catch(err) {
      var jqueryLoaded=false;
  }
  if (!jqueryLoaded) {
      var head= document.getElementsByTagName('head')[0];
      var script= document.createElement('script');
      script.type= 'text/javascript';
      script.src= 'http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js';
      head.appendChild(script);
  }
  </script>
  <script type="text/javascript" src="http://downloads.mailchimp.com/js/jquery.form-n-validate.js"></script>

  <script type="text/javascript">
  var fnames = new Array();var ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='NAME';ftypes[1]='text';fnames[3]='COMPANY';ftypes[3]='text';fnames[2]='COUNTRY';ftypes[2]='dropdown';var err_style = '';
  try{
      err_style = mc_custom_error_style;
  } catch(e){
      err_style = 'margin: 1em 0 0 0; padding: 1em 0.5em 0.5em 0.5em; background: ERROR_BGCOLOR none repeat scroll 0% 0%; font-weight: bold; float: left; z-index: 1; width: 80%; -moz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial; color: ERROR_COLOR;';
  }
  var head= document.getElementsByTagName('head')[0];
  var style= document.createElement('style');
  style.type= 'text/css';
  if (style.styleSheet) {
    style.styleSheet.cssText = '.mce_inline_error {' + err_style + '}';
  } else {
    style.appendChild(document.createTextNode('.mce_inline_error {' + err_style + '}'));
  }
  head.appendChild(style);
  $(document).ready( function($) {
    var options = { errorClass: 'mce_inline_error', errorElement: 'div', onkeyup: function(){}, onfocusout:function(){}, onblur:function(){}  };
    var mce_validator = $("#mc-embedded-subscribe-form").validate(options);
    options = { url: 'http://drupal.us2.list-manage.com/subscribe/post-json?u=618af7a460cf93cf211231139&id=189afc7b7a&c=?', type: 'GET', dataType: 'json', contentType: "application/json; charset=utf-8",
                  beforeSubmit: function(){
                      $('#mce_tmp_error_msg').remove();
                      $('.datefield','#mc_embed_signup').each(
                          function(){
                              var txt = 'filled';
                              var fields = new Array();
                              var i = 0;
                              $(':text', this).each(
                                  function(){
                                      fields[i] = this;
                                      i++;
                                  });
                              $(':hidden', this).each(
                                  function(){
                                  if ( fields[0].value=='MM' && fields[1].value=='DD' && fields[2].value=='YYYY' ){
                                  this.value = '';
  } else if ( fields[0].value=='' && fields[1].value=='' && fields[2].value=='' ){
                                  this.value = '';
  } else {
                                      this.value = fields[0].value+'/'+fields[1].value+'/'+fields[2].value;
                                  }
                                  });
                          });
                      return mce_validator.form();
                  },
                  success: mce_success_cb
              };
    $('#mc-embedded-subscribe-form').ajaxForm(options);

  });
  function mce_success_cb(resp){
      $('#mce-success-response').hide();
      $('#mce-error-response').hide();
      if (resp.result=="success"){
          $('#mce-'+resp.result+'-response').show();
          $('#mce-'+resp.result+'-response').html(resp.msg);
          $('#mc-embedded-subscribe-form').each(function(){
              this.reset();
      });
      } else {
          var index = -1;
          var msg;
          try {
              var parts = resp.msg.split(' - ',2);
              if (parts[1]==undefined){
                  msg = resp.msg;
              } else {
                  i = parseInt(parts[0]);
                  if (i.toString() == parts[0]){
                      index = parts[0];
                      msg = parts[1];
                  } else {
                      index = -1;
                      msg = resp.msg;
                  }
              }
          } catch(e){
              index = -1;
              msg = resp.msg;
          }
          try{
              if (index== -1){
                  $('#mce-'+resp.result+'-response').show();
                  $('#mce-'+resp.result+'-response').html(msg);
              } else {
                  err_id = 'mce_tmp_error_msg';
                  html = '<div id="'+err_id+'" style="'+err_style+'"> '+msg+'</div>';

                  var input_id = '#mc_embed_signup';
                  var f = $(input_id);
                  if (ftypes[index]=='address'){
                      input_id = '#mce-'+fnames[index]+'-addr1';
                      f = $(input_id).parent().parent().get(0);
                  } else if (ftypes[index]=='date'){
                      input_id = '#mce-'+fnames[index]+'-month';
                      f = $(input_id).parent().parent().get(0);
                  } else {
                      input_id = '#mce-'+fnames[index];
                      f = $().parent(input_id).get(0);
                  }
                  if (f){
                      $(f).append(html);
                      $(input_id).focus();
                  } else {
                      $('#mce-'+resp.result+'-response').show();
                      $('#mce-'+resp.result+'-response').html(msg);
                  }
              }
          } catch(e){
              $('#mce-'+resp.result+'-response').show();
              $('#mce-'+resp.result+'-response').html(msg);
          }
      }
  }
  </script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-2360451-5']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</body>
</html>
