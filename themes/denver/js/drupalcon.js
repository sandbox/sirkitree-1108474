$(document).ready(function() { 
  
  $(window).resize(function() {
    resizeFullImages();
  });
  
  $(window).scroll(function () {
    positionContent();
  });
  
  $("#nav ul li a").each(function(){
    $(this).click(function(){
      scrollContent($(this).attr("href"));
      return false;
    })
  })
  
  $(".gallery").each(function(){
    addSlideshow($(this));
  })
    
  resizeFullImages();
  positionContent();
    
});

function resizeFullImages() {
  $(".full-image").width(Math.max(600,$(window).width()));
}

function scrollContent(target) {
  $('html,body').animate({scrollTop: $(target).offset().top}, 1000);
}

function setActiveLink(target) {
  $("#nav ul li a").each(function(){
    $(this).removeClass("active");
    if($(this).attr("href") == target){
      $(this).addClass("active");
    }
  });
}

function positionContent() {

  var windowHeight = $(window).height();
  var offset = $(window).scrollTop();
  var userAgent = window.navigator.userAgent;

  if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i) || userAgent.match(/android/i)) {
    
    $('#nav').css({position: 'absolute'});
    $('#bg-mountains').css({position: 'absolute'});
    
  } // if
  else {
    
    if( offset >= 400 && offset < 1000){
      setActiveLink("#drupalElevated");
    } else if( offset >= 1000 && offset < 3350 ){
      setActiveLink("#denver");
    } else if(offset >= 3350 && offset < 3900){
      setActiveLink("#conventionCenter");
    } else if(offset >= 3900 && offset < 4800){
      setActiveLink("#community");
    } else {
      setActiveLink("");
    }
    
    //  Slide 1 - Intro - 400
    
    $("#slide1 .drop").css({
      top: (20 - (offset/400)*300) + "px"
    });
    $("#slide1 h1").css({
      top: (150 - (offset/400)*200) + "px"
    });
    $("#slide1 h2").css({
      top: (290 - (offset/400)*100) + "px"
    });
    
    //  Slide 2 - Drupal Elevated - 600 - 1000
    
    $("#slide2 .content").css({
      "padding-top": (50 + ((Math.max(0, 400 - offset))/400)*100) + "px"
    });
    $("#slide2 #beyond").css({
      bottom: (0 - ((Math.max(0, 400 - offset))/400)*200) + "px"
    });
    
    $("#slide2 #badge").css({
      left: (-20 + ((Math.max(0, 400 - offset))/400)*40) + "px"
    });
    $("#slide2 #linuxJournal").css({
      bottom: (100 - ((Math.max(0, 400 - offset))/400)*400) + "px"
    });
    $("#slide2 #dcfsPhoto").css({
      bottom: (0 - ((Math.max(0, 400 - offset))/400)*300) + "px"
    });
    $("#slide2 #launchParties").css({
      bottom: (0 - ((Math.max(0, 400 - offset))/400)*200) + "px"
    });
    $("#slide2 #drupalWebsite").css({
      bottom: (0 - ((Math.max(0, 400 - offset))/400)*100) + "px"
    });
    
    //  Slide 3 - Denver - 900 - 1900
    
    $("#slide3 h2").css({
      "padding-top": (40 + ((Math.max(0, 400 - (offset - 600)))/400)*300) + "px"
    });
    
    //  Slide 4 - Location - 800 - 2700
    
    $("#slide4 .content").css({
      "padding-top": (50 + ((Math.max(0, 200 - (offset - 1400)))/200)*200) + "px"
    });
    
    $("#slide4 #map").css({
      top: (0 + ((Math.max(0, 200 - (offset - 1500)))/200)*200) + "px",
      opacity: (1 - ((Math.max(0, 400 - (offset - 1500)))/400)*1)
    });
    
    //  Slide 5 - Business - 650 - 3350
  
    $("#slide5 #business01").css({
      left: (298 - ((Math.max(0, 400 - (offset - 2200)))/400)*400) + "px"
    });
    $("#slide5 #business02").css({
      right: (0 - ((Math.max(0, 400 - (offset - 2250)))/400)*400) + "px"
    });
    $("#slide6 #business03").css({
      left: (0 - ((Math.max(0, 400 - (offset - 2300)))/400)*400) + "px"
    });
  
    //  Slide 6 - Convention Center - 550 - 3900
    
    //  Slide 7 - Community - 500 - 4400
    
    $("#slide7 .content").css({
      "padding-top": (50 + ((Math.max(0, 400 - (offset - 3700)))/400)*300) + "px"
    });
    
    $("#slide7 #stat01-dt").css({
      left: (0 + ((Math.max(0, 400 - (offset - 3700)))/400)*1000) + "px"
    });
    $("#slide7 #stat01-dd").css({
      left: (150 + ((Math.max(0, 400 - (offset - 3700)))/400)*1000) + "px"
    });
    $("#slide7 #stat02-dt").css({
      left: (0 + ((Math.max(0, 400 - (offset - 3750)))/400)*1000) + "px"
    });
    $("#slide7 #stat02-dd").css({
      left: (150 + ((Math.max(0, 400 - (offset - 3750)))/400)*1000) + "px"
    });
    
    $("#slide7 #stat03-dt").css({
      left: (0 + ((Math.max(0, 400 - (offset - 3800)))/400)*1000) + "px"
    });
    $("#slide7 #stat03-dd").css({
      left: (150 + ((Math.max(0, 400 - (offset - 3800)))/400)*1000) + "px"
    });
    
    $("#slide7 #stat04-dt").css({
      left: (0 + ((Math.max(0, 400 - (offset - 3850)))/400)*1000) + "px"
    });
    $("#slide7 #stat04-dd").css({
      left: (150 + ((Math.max(0, 400 - (offset - 3850)))/400)*1000) + "px"
    });
    
    //  Slide 8 - Volunteer - 900 - 4800
    
    //  Slide 9 - Closing - 700 - 5500
        
    $("#slide9 .drop").css({
      top: (20 + ((Math.max(0, 400 - (offset - 5000 + (windowHeight - 700))))/400)*100) + "px"
    });
    
    $("#slide9 h1").css({
      top: (150 + ((Math.max(0, 400 - (offset - 5100 + (windowHeight - 700))))/400)*200) + "px"
    });
    
    $("#slide9 h2").css({
      top: (290 + ((Math.max(0, 400 - (offset - 5200 + (windowHeight - 700))))/400)*300) + "px"
    });
    
    $("#slide9 .twitter").css({
      top: (400 + ((Math.max(0, 400 - (offset - 5300 + (windowHeight - 700))))/400)*400) + "px"
    });

  } // else
  
}


function addSlideshow(target){
  $("<a class='next' href='#'>Next</a>").click(function(){
    switchSlide(target, 1);
    return false;
  }).appendTo(target);
  $("<a class='back' href='#'>Back</a>").click(function(){
    switchSlide(target, -1);
    return false;
  }).appendTo(target);
}

function switchSlide(target, direction) {
  
  var photos = target.find(".photos li");
  var current = 0;
  
  photos.each(function(index){
    $(this).removeClass("previous");
    if($(this).hasClass("current")){
      current = index;
    }
  })
  
  nextSlide = current + direction;
  
  if(nextSlide < 0){
    nextSlide = photos.length - 1;
  }else if (nextSlide >= photos.length){
    nextSlide = 0;
  }
  
  photos.eq(nextSlide).addClass("current").css("display", "none").fadeIn();
  photos.eq(current).removeClass("current").addClass("previous");
}


