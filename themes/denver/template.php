<?php

function denver_process_html(&$vars) {
  if ($node = menu_get_object()) {
    $vars['theme_hook_suggestions'][] = 'html__'. $node->type;
  }
}