(function ($) {
  
  $(document).ready(function(){
    
    // Highlighted Slideshow
    if ($('#highlighted').length > 0) {
      makeSlideshow($("#highlighted").find("ul.list"));
    } //if #highlighted
    
  }); //document.ready

  function makeSlideshow(t){
    var slideshow = $(t);
    $(t).wrap("<div class='slideshow-wrapper'></div>");
    var slideshowWrapper = $(t).parent();
    
    if($(".list-item", slideshow).length > 1){

      slideshow.addClass("slideshow");

      $("<a href='#' class='slide-previous'>Previous</a>").click(function(){
        switchSlide(t, -1);
        return false;
      }).prependTo(slideshowWrapper);
      $("<a href='#' class='slide-next'>Next</a>").click(function(){
        switchSlide(t, 1);
        return false;
      }).prependTo(slideshowWrapper);

      $(".list-item", slideshow).each(function(index){
        if(index == 0){
          $(this).addClass("current");
          $(this).fadeIn("fast");
        }
      });
    }
  }
  
  function switchSlide(t, direction){
    slidecounter = 0;
    var slideshow = $(t);
    if(!slideshow.hasClass("switching")){

      slideshow.addClass("switching");

      var slides = $(".list-item", slideshow);
      var totalSlides = slides.length;
      var currentSlide = 0;
      slides.each(function(index){
        if($(this).hasClass("current")){
          currentSlide = index;
        }
      })

      var nextSlide = currentSlide + direction;
      if(nextSlide < 0){
        nextSlide = totalSlides - 1;
      }else if (nextSlide >= totalSlides){
        nextSlide = 0;
      }

      slides.eq(currentSlide).removeClass("current");
      slides.eq(currentSlide).addClass("fading");
      slides.eq(nextSlide).addClass("current");
      slides.eq(currentSlide).fadeOut(300, function(){
        $(this).removeClass("fading");
        slides.eq(nextSlide).css("visibility", "visible");
        slides.eq(nextSlide).fadeIn(300, function(){
          slideshow.removeClass("switching");
        })
      });
    }
  }

})(jQuery); //$
