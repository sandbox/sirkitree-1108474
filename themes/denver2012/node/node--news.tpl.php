<?php
  //!empty($content['upload']) && hide($content['upload']);
  //!empty($content['taxonomy_vocabulary_1']) && hide($content['taxonomy_vocabulary_1']);
  !empty($content['links']) && hide($content['links']);
  !empty($content['comments']) && hide($content['comments']);
  !empty($content['field_news_tags']) && hide($content['field_news_tags']);
?>
<div class="node clearfix node-<?php print $node->type; ?>">
  <div class="meta">
    <span class="post-date">
      <span class="post-month"><?php print format_date($node->created, 'custom', 'M'); ?></span>
      <span class="post-day"><?php print format_date($node->created, 'custom', 'j'); ?></span>
      <span class="post-year"><?php print format_date($node->created, 'custom', 'Y'); ?></span>
    </span>
    <span class="comment-count">
      <?php print l($node->comment_count, "node/".$node->nid, array("fragment" => "comments")); ?>
    </span>
  </div>
  <div class="news-content">
    
      <?php print render($title_prefix); ?>
      <?php if (!$page) { ?>
        <h3>
      <?php } else { ?>
        <h1>
      <?php } ?>
        <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
      <?php if (!$page) { ?>
        </h3>
      <?php } else { ?>
        </h1>
      <?php } ?>
      <?php print render($title_suffix); ?>
  
    <?php
      print render($content);
      print render($content['links']);
    ?>
    <?php if(!empty($content['field_news_tags'])) { ?>
      <div class="tags">
        <?php print render($content['field_news_tags']); ?>
      </div>
    <?php } ?>
    <?php  
      print render($content['comments']);
    ?>
  </div>
</div>