<?php
  //!empty($content['upload']) && hide($content['upload']);
  //!empty($content['taxonomy_vocabulary_1']) && hide($content['taxonomy_vocabulary_1']);
  !empty($content['links']) && hide($content['links']);
  !empty($content['comments']) && hide($content['comments']);
  !empty($content['field_training_cost']) && hide($content['field_training_cost']);
  !empty($content['field_training_cost_combo']) && hide($content['field_training_cost_combo']);
  !empty($content['field_training_availability']) && hide($content['field_training_availability']);
  $training_combo_title = $content['field_training_cost_combo']["#title"];
  $training_combo_value = $content['field_training_cost_combo'][0]["#markup"];
  $training_cost_title = $content['field_training_cost']["#title"];
  $training_cost_value = $content['field_training_cost'][0]["#markup"];
  $training_available = $content['field_training_availability']["#items"][0]["value"] == "0" ? "sold-out" : "available";
  //dpm($content);
?>
<div class="node clearfix node-<?php print $node->type; ?>">
  <?php if (!$page) { ?>
    <?php print render($title_prefix); ?>
    <h3>
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h3>
    <?php print render($title_suffix); ?>
  <?php } ?>
  <?php if ($submitted) { ?>
    <div class="meta">
      <span class="submitted">
        <?php print format_date($node->created); ?>
      </span>
    </div>
  <?php } ?>
  <?php
    print render($content);
  ?>
  <?php if(($training_cost_value != "$0.00" || $training_combo_value != "$0.00") || ($training_available == "sold-out")){ ?>
  <h4 class="register-title">Register</h4>
  <ul class="training-cost training-<?php print $training_available; ?>">
    <?php if($training_combo_value != "$0.00"){ ?>
      <li><strong><?php print $training_combo_title; ?>:</strong> <a href="http://association.drupal.org/denver2012"><?php print $training_combo_value; ?></a></li>
    <? } ?>
    <?php if($training_cost_value != "$0.00"){ ?>
      <li><strong><?php print $training_cost_title; ?>:</strong> <a href="http://association.drupal.org/denver2012"><?php print $training_cost_value; ?></a></li>
    <? } ?>
  </ul>
  <? } ?>
  
  <?php
    print render($content['links']);
  ?>
  <?php  
    print render($content['comments']);
  ?>
</div>