<div id="wrapper">
  <?php if ($page['utility']) { ?>
    <div id="utility">
      <div class="container">
        <?php print render($page['utility']); ?>
      </div>
    </div>
  <? } // end utility ?>
  <?php if ($page['navigation']) { ?>
    <div id="navigation">
      <div class="container">
        <?php print render($page['navigation']); ?>
      </div>
    </div>
  <? } // end utility ?>
  <div id="header">
    <div class="container">
      <h1 id="logo">
        <a href="<?php print $base_path; ?>"><?php print $site_name; ?> <time datetime="2012-03-19">March 19-23, 2012</time></a>
      </h1>
      <h2 id="theme">Collaborative Publishing for Every Device</h2>
      <?php if ($page['header']) { ?>
          <?php print render($page['header']); ?>
      <? } // end header ?>
    </div>
  </div>
  
  <?php if ($page['highlighted']) { ?>
    <div id="highlighted">
      <div class="container">
        <?php print render($page['highlighted']); ?>
      </div>
    </div>
  <? } // end sidebar_first ?>
  
  <div id="main">
    <div class="container">
      
      <?php if ($messages) { ?>
        <div id="messages">
            <?php print $messages; ?>
        </div>
      <? } // end messages ?>
      
      <div id="main-content">
        <div id="content-wrapper">
          <?php if (render($tabs)) { ?>
            <div id="tabs">
              <?php print render($tabs); ?>
            </div>
          <? } // end tabs ?>
          <div id="content">
            <?php if (!$is_front && strlen($title) > 0 && ((!empty($node) && $node->type != "news") || (empty($node)))) { ?>
              <h1><?php print $title; ?></h1> 
            <? } ?>
            <?php if ($page['help']) { ?>
              <div id="help">
                <?php print render($page['help']); ?>
              </div>
            <? } // end tabs ?>
            <?php print render($page['content']); ?>
          </div>
        </div>
      
        <?php if ($page['sidebar_first']) { ?>
          <div id="sidebar-first" class="aside">
            <?php print render($page['sidebar_first']); ?>
          </div>
        <? } // end sidebar_first ?>
      </div>
      <?php if ($page['sidebar_second']) { ?>
        <div id="sidebar-second" class="aside">
          <?php print render($page['sidebar_second']); ?>
        </div>
      <? } // end sidebar_second ?>
    </div>
  </div>
  <div id="footer">
    <div class="container">
      <?php print render($page['footer']); ?>
    </div>
  </div>
</div>
<img id="bg-mountains" class="full-image" src="/sites/all/themes/denver2012/images/bg-mountains.jpg" alt="Colorado mountains">