<?php

function denver2012_preprocess_html(&$variables, $hook) {
  // Classes for body element. Allows advanced theming based on context
  // (home page, node of certain type, etc.)
  if (!$variables['is_front']) {
    // Add unique class for each page.
    if (isset($_GET['q'])) {
      $path = drupal_get_path_alias($_GET['q']);
      // Add unique class for each website section.
      list($section, ) = explode('/', $path, 2);
      if (arg(0) == 'node') {
        if (arg(1) == 'add') {
          $section = 'node-add';
        }
        elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
          $section = 'node-' . arg(2);
        }
      }
      $variables['classes_array'][] = drupal_html_class('section-' . $section);
    }
  }

  $variables['drupalorg_site_status'] = filter_xss_admin(variable_get('drupalorg_site_status', FALSE));
}


function denver2012_tablesort_indicator($variables) {
  if ($variables['style'] == "asc") {
    return theme('image', array('path' => '/sites/all/themes/denver2012/images/arrow-asc.png', 'width' => 16, 'height' => 16, 'alt' => t('sort ascending'), 'title' => t('sort ascending')));
  }
  else {
    return theme('image', array('path' => '/sites/all/themes/denver2012/images/arrow-desc.png', 'width' => 16, 'height' => 16, 'alt' => t('sort descending'), 'title' => t('sort descending')));
  }
}

