<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge, chrome=1" />
<title><?php print $head_title; ?></title>
<link rel="shortcut icon" type="image/x-icon" href="/sites/all/themes/nucleus/assets/images/favicon.ico" />
<?php print $styles; ?>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php
    print $page_top;
    print $page;
    print $scripts;
    print $page_bottom;
  ?>
</body>
</html>