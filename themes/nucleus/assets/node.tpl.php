<?php
  //!empty($content['upload']) && hide($content['upload']);
  //!empty($content['taxonomy_vocabulary_1']) && hide($content['taxonomy_vocabulary_1']);
  !empty($content['links']) && hide($content['links']);
?>
<div class="node clearfix node-<?php print $node->type; ?>">
  <?php if (!$page) { ?>
    <?php print render($title_prefix); ?>
    <h3>
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h3>
    <?php print render($title_suffix); ?>
  <?php } ?>
  <?php if ($submitted) { ?>
    <div class="meta">
      <span class="submitted">
        <?php print format_date($node->created); ?>
      </span>
    </div>
  <?php } ?>
  <?php
    print render($content);
    print render($content['links']);
  ?>
</div>