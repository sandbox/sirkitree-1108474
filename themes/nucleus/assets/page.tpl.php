<div id="wrapper">
  <div id="header">
    <div class="container">
      <h1 id="logo">
        <a href="<?php print $base_path; ?>"><?php print $site_name; ?></a>
      </h1>
      <?php if ($page['header']) { ?>
          <?php print render($page['header']); ?>
      <? } // end header ?>
    </div>
  </div>
  
  <?php if ($page['highlighted']) { ?>
  <div id="highlighted">
      <div class="container">
        <?php print render($page['highlighted']); ?>
      </div>
    </div>
  <? } // end sidebar_first ?>
  
  <div id="main">
    <div class="container">
      <?php if (!$is_front && strlen($title) > 0) { ?>
        <h1 class="page-title"><?php print $title; ?></h1> 
      <? } ?>
      
      <?php if (render($tabs)) { ?>
        <?php print render($tabs); ?>
      <? } // end tabs ?>
      
      <?php if ($messages) { ?>
        <div id="messages">
            <?php print $messages; ?>
        </div>
      <? } // end messages ?>
      
      
      
      <div id="main-content">
        <div id="content">
          <?php print render($page['content']); ?>
        </div>
      
        <?php if ($page['sidebar_first']) { ?>
          <div id="sidebar-first" class="aside">
            <?php print render($page['sidebar_first']); ?>
          </div>
        <? } // end sidebar_first ?>
      </div>
      <?php if ($page['sidebar_second']) { ?>
        <div id="sidebar-second" class="aside">
          <?php print render($page['sidebar_second']); ?>
        </div>
      <? } // end sidebar_second ?>
    </div>
  </div>
  <div id="footer">
    <div class="container">
      <span class="copyright">&copy; <?php print format_date(time(), 'custom', 'Y') . ' ' . $site_name; ?></span>
      <?php print render($page['footer']); ?>
    </div>
  </div>
</div>